//
//  AppDelegate.h
//  Brad-qr
//
//  Created by Devanshu Saini on 06/01/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) ViewController *firstController;

@end

