//
//  ViewController.m
//  Brad-qr
//
//  Created by Devanshu Saini on 06/01/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITextFieldDelegate>

@end

@implementation ViewController{
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UILabel *lblFirstname;
    
    IBOutlet UILabel *lblLastname;
    
    IBOutlet UILabel *lblEmail;
    
    IBOutlet UILabel *lblContact;
    
    IBOutlet UILabel *lblPostalCode;
    
    IBOutlet UIButton *btnGenerateQRCode;
    UIAlertController* alert;
}
@synthesize txtPostalCode, txtContact, txtEmail, txtLastname, txtFirstname;

- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setViewItems];
    #ifdef DEBUG
        [self setDummyData];
    #endif
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateNavigationBar];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (NSString *)removeWhiteSpaceFromTextField:(UITextField *)textField{
    NSString *trimmedString = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    textField.text = trimmedString;
    return trimmedString;
}

- (void)setDummyData{
    txtFirstname.text = @"Devanshu";
    txtLastname.text = @"Saini";
    txtEmail.text = @"devanshu@macroapps.com";
    txtContact.text = @"+918447523757";
    txtPostalCode.text = @"110010";
}

- (UITextField *)getInvalidTextField:(NSString **)error{
    NSString *firstName = [self removeWhiteSpaceFromTextField:txtFirstname];
    if (!firstName.length) {
        *error = [Utils localForKey:@"PROVIDE_FIRST_NAME"];
        return txtFirstname;
    }
    
    NSString *lastName = [self removeWhiteSpaceFromTextField:txtLastname];
    if (!lastName.length) {
        *error = [Utils localForKey:@"PROVIDE_LAST_NAME"];
        return txtLastname;
    }
    
    NSString *email = [self removeWhiteSpaceFromTextField:txtEmail];
    if (!email.length) {
        *error = [Utils localForKey:@"PROVIDE_EMAIL"];
        return txtEmail;
    }
    if (![Utils validateStringForEmail:email]) {
        *error = [Utils localForKey:@"PROVIDE_VALID_EMAIL"];
        return txtEmail;
    }
    
    NSString *contact = [self removeWhiteSpaceFromTextField:txtContact];
    if (!contact.length) {
        *error = [Utils localForKey:@"PROVIDE_CONTACT"];
        return txtContact;
    }
    
    NSString *postalCode = [self removeWhiteSpaceFromTextField:txtPostalCode];
    if (!postalCode.length) {
        *error = [Utils localForKey:@"PROVIDE_POSTAL_CODE"];
        return txtPostalCode;
    }
    return nil;
}

- (IBAction)generateQRCodeButtonTap:(id)sender{
    [self resignKeyboards];
    NSString *error = nil;
    UITextField *errorField = [self getInvalidTextField:&error];
    if (errorField) {
        alert = [UIAlertController alertControllerWithTitle:[Utils localForKey:@"ERROR_ALERT_TITLE"] message:error preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:[Utils localForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [errorField becomeFirstResponder];
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        FormQRUserModal *qrUserModal = [[FormQRUserModal alloc] init];
        qrUserModal.firstName = txtFirstname.text;
        qrUserModal.lastName = txtLastname.text;
        qrUserModal.email = txtEmail.text;
        qrUserModal.contact = txtContact.text;
        qrUserModal.postalCode = txtPostalCode.text;
        QRViewController *qrViewController = [[QRViewController alloc] initWithNibName:@"QRViewController" bundle:nil];
        qrViewController.qrUserModal = qrUserModal;
        [self.navigationController pushViewController:qrViewController animated:YES];
    }
}

- (IBAction)resignKeyboards{
    [txtFirstname resignFirstResponder];
    [txtLastname resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtPostalCode resignFirstResponder];
    [txtContact resignFirstResponder];
}

#pragma mark - UITextfield Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if ([textField isEqual:txtFirstname]) {
        [txtLastname becomeFirstResponder];
    }
    else if ([textField isEqual:txtLastname]) {
        [txtEmail becomeFirstResponder];
    }
    else if ([textField isEqual:txtEmail]) {
        [txtContact becomeFirstResponder];
    }
    else if ([textField isEqual:txtContact]) {
        [txtPostalCode becomeFirstResponder];
    }
    else if ([textField isEqual:txtPostalCode]) {
        [self generateQRCodeButtonTap:nil];
    }
    return YES;
}

#pragma mark - View Rendering

- (void)updateNavigationBar{
    self.navigationItem.title = [Utils localForKey:@"HOME_SCREEN_TITLE"];
}

- (void)setDefaultFormattingForTextField:(UITextField *)textField{
    [textField setFont:DEFAULT_TXT_FONT];
    textField.textColor = DEFAULT_TXT_COLOR;
    textField.layer.borderColor = DEFAULT_TXT_BORDER_CGCOLOR;
    textField.layer.borderWidth = 1.0;
    textField.layer.cornerRadius = DEFAULT_TXT_CORNER_RADIUS;
    textField.leftView = textField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 5.0, 35.0)];
    textField.leftViewMode = textField.rightViewMode = UITextFieldViewModeAlways;
}

- (void)setDefaultFormattingtForLabel:(UILabel *)label{
    [label setFont:DEFAULT_LBL_FONT];
    label.textColor = DEFAULT_LBL_COLOR;
}

- (void)setViewItems{
    [self setDefaultFormattingForTextField:txtFirstname];
    [txtFirstname setPlaceholder:[Utils localForKey:@"YOUR_FIRST_NAME"]];
    
    [self setDefaultFormattingtForLabel:lblFirstname];
    lblFirstname.text = [Utils localForKey:@"FIRST_NAME"];
    
    [self setDefaultFormattingForTextField:txtLastname];
    [txtLastname setPlaceholder:[Utils localForKey:@"YOUR_LAST_NAME"]];
    
    [self setDefaultFormattingtForLabel:lblLastname];
    lblLastname.text = [Utils localForKey:@"LAST_NAME"];
    
    [self setDefaultFormattingForTextField:txtEmail];
    [txtEmail setPlaceholder:[Utils localForKey:@"YOUR_EMAIL"]];
    txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    
    [self setDefaultFormattingtForLabel:lblEmail];
    lblEmail.text = [Utils localForKey:@"EMAIL"];
    
    [self setDefaultFormattingForTextField:txtContact];
    [txtContact setPlaceholder:[Utils localForKey:@"YOUR_PHONE_NUMBER"]];
    txtContact.keyboardType = UIKeyboardTypePhonePad;
    
    [self setDefaultFormattingtForLabel:lblContact];
    lblContact.text = [Utils localForKey:@"PHONE_NUMBER"];
    
    [self setDefaultFormattingForTextField:txtPostalCode];
    [txtPostalCode setPlaceholder:[Utils localForKey:@"YOUR_POSTAL_CODE"]];
    txtPostalCode.keyboardType = UIKeyboardTypePhonePad;
    
    [self setDefaultFormattingtForLabel:lblPostalCode];
    lblPostalCode.text = [Utils localForKey:@"POSTAL_CODE"];
    
    [btnGenerateQRCode setTitle:[Utils localForKey:@"GENERATE_QR_CODE"] forState:UIControlStateNormal];
    [btnGenerateQRCode setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnGenerateQRCode setBackgroundColor:BLUE_BACKGROUND_1];
    btnGenerateQRCode.layer.cornerRadius = DEFAULT_TXT_CORNER_RADIUS;
}

@end
