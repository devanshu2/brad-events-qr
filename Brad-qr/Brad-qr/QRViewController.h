//
//  QRViewController.h
//  Brad-qr
//
//  Created by Devanshu Saini on 07/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormQRUserModal.h"
#import "Utils.h"
#import "UIImage+MDQRCode.h"
#import "constants.h"

@interface QRViewController : UIViewController <UIPrintInteractionControllerDelegate>

@property (nonatomic, strong) FormQRUserModal *qrUserModal;

@end
