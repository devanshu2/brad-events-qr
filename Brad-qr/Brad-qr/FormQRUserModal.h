//
//  FormQRUserModal.h
//  Brad-qr
//
//  Created by Devanshu Saini on 07/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormQRUserModal : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *contact;
@property (nonatomic, strong) NSString *postalCode;

@end
