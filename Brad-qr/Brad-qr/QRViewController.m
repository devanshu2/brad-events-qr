//
//  QRViewController.m
//  Brad-qr
//
//  Created by Devanshu Saini on 07/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#import "QRViewController.h"

@interface QRViewController ()

@end

@implementation QRViewController{
    IBOutlet UIImageView *qrCodeImageView;
    IBOutlet UILabel *userName;
    IBOutlet UIView *qrWrapper;
    UIActivityViewController *printActivityVC;
    UIBarButtonItem *navPrint;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [self updateNavigationBar];
    [self setQRView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    printActivityVC = [self getClassPrintActivityVC];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - View Rendering

- (void)setQRView{
    userName.text = _qrUserModal.firstName;
    userName.textColor = [UIColor blackColor];//DEFAULT_LBL_COLOR;
    userName.font = QR_TXT_FONT;
    NSString *qrCodeString = [NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@", [Utils localForKey:@"QR_FIRST_NAME"], _qrUserModal.firstName, [Utils localForKey:@"QR_LAST_NAME"], _qrUserModal.lastName, [Utils localForKey:@"QR_EMAIL"], _qrUserModal.email, [Utils localForKey:@"QR_PHONE_NUMBER"], _qrUserModal.contact, [Utils localForKey:@"QR_POSTAL_CODE"], _qrUserModal.postalCode];
    UIImage *qrImage = [UIImage mdQRCodeForString:qrCodeString size:300.0 fillColor:[UIColor blackColor]];
    qrCodeImageView.image = qrImage;
}

- (UIImage *)getImageToPrint{
    UIView *subView = qrWrapper;
    UIGraphicsBeginImageContextWithOptions(subView.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [subView.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

- (void)updateNavigationBar{
    self.navigationItem.title = [Utils localForKey:@"QRVIEW_TITLE"];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav-back"] style:UIBarButtonItemStylePlain target:self action:@selector(goBackCall)];
    navPrint = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav-print"] style:UIBarButtonItemStylePlain target:self action:@selector(printButtonCall)];
    UIBarButtonItem * navShare = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav-share"] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonCall)];
    self.navigationItem.rightBarButtonItems = @[navShare, navPrint];
    self.navigationItem.leftBarButtonItem = backButton;
}

#pragma mark - Actions

- (void)printButtonCall{
    [self printImage:[self getImageToPrint]];
}

- (UIActivityViewController *)getClassPrintActivityVC{
    static UIActivityViewController *activityVC = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[[self getImageToPrint]] applicationActivities:nil];
    });
    return activityVC;
}

- (void)shareButtonCall{
    printActivityVC = [self getClassPrintActivityVC];
    printActivityVC.excludedActivityTypes = @[];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:printActivityVC animated:YES completion:nil];
    });
}

- (void)printImage:(UIImage *)myImage {
    UIPrintInteractionController *controller = [UIPrintInteractionController  sharedPrintController];
    if(!controller){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Utils localForKey:@"ERROR_ALERT_TITLE"] message:@"Could not get Airprint Services.\nPlease try later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:[Utils localForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    printInfo.outputType = UIPrintInfoOutputPhoto;
    printInfo.jobName = @"Badge Generator Print";
    printInfo.duplex = UIPrintInfoDuplexNone;
    controller.printingItem = myImage;
    controller.showsPaperSelectionForLoadedPapers = YES;
    
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
    ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
        if (!completed && error) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[Utils localForKey:@"ERROR_ALERT_TITLE"] message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:[Utils localForKey:@"OK"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    };
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [controller presentFromBarButtonItem:navPrint animated:YES completionHandler:completionHandler];
    } else {
        [controller presentAnimated:YES completionHandler:completionHandler];
    }
}

- (void)goBackCall{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
