//
//  Utils.m
//  Brad-qr
//
//  Created by Devanshu Saini on 07/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString *)localForKey: (NSString *)key{
    return NSLocalizedStringFromTable(key, @"Localization", nil);
}

+ (BOOL)validateStringForEmail:(NSString *)checkString{
    //BOOL stricterFilter = NO;
    //NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @"[^@]+@([^@]+)";
    //NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", laxString];
    return [emailTest evaluateWithObject:checkString];
}

@end
