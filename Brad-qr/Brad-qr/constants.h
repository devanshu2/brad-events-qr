//
//  constants.h
//  Brad-qr
//
//  Created by Devanshu Saini on 06/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#ifndef constants_h
#define constants_h

#define DEFAULT_TXT_CORNER_RADIUS 4.0
#define DEFAULT_TXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:12.0]
#define DEFAULT_TXT_COLOR [UIColor colorWithWhite:0.6 alpha:1.0]
#define DEFAULT_TXT_BORDER_CGCOLOR [[UIColor colorWithWhite:0.7 alpha:1.0] CGColor]
#define DEFAULT_LBL_FONT [UIFont fontWithName:@"HelveticaNeue" size:14.0]
#define DEFAULT_LBL_COLOR [UIColor colorWithWhite:0.6 alpha:1.0]
#define QR_TXT_FONT [UIFont fontWithName:@"HelveticaNeue-Bold" size:30.0]
#define BLUE_BACKGROUND_1 [UIColor colorWithRed:(31.0/255.0) green:(187.0/255.0) blue:(239.0/255.0) alpha:1.0]

#endif /* constants_h */
