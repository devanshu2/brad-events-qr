//
//  ViewController.h
//  Brad-qr
//
//  Created by Devanshu Saini on 06/01/16.
//  Copyright © 2016 Devanshu Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import "Utils.h"
#import "QRViewController.h"
@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *txtFirstname;

@property (nonatomic, strong) IBOutlet UITextField *txtLastname;

@property (nonatomic, strong) IBOutlet UITextField *txtEmail;

@property (nonatomic, strong) IBOutlet UITextField *txtContact;

@property (nonatomic, strong) IBOutlet UITextField *txtPostalCode;

@end

