//
//  Utils.h
//  Brad-qr
//
//  Created by Devanshu Saini on 07/01/16.
//  Copyright © 2016 Devenshu Saini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString *)localForKey: (NSString *)key;

+ (BOOL)validateStringForEmail:(NSString *)checkString;

@end
